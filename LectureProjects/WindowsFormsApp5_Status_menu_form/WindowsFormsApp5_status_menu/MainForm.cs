﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5_status_menu {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void openMyDialogToolStripMenuItem_Click(object sender, EventArgs e) {
            // https://stackoverflow.com/questions/3097364/c-sharp-form-close-vs-form-dispose
            using (MyDialog md = new MyDialog())
                md.ShowDialog(); // When using ShowDialog remember to Dispose or keep the dialog
        }

        private void showMyDialogToolStripMenuItem_Click(object sender, EventArgs e) {
            MyDialog md = new MyDialog();
            md.Show();            
        }
    }
}
