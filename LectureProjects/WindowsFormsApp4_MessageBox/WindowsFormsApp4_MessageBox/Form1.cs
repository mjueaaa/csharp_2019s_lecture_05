﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4_MessageBox {
    public partial class Form1 : Form {
        public Form1() {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InvariantCulture;

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            const string message = "I am a message from a MessageBox";
            const string caption = "Caption for MessageBox";
            var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNoCancel,
                                         MessageBoxIcon.Exclamation);

            MessageBox.Show(result.ToString());           

        }
    }
}
