﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1 {

    public partial class MyClass {
        public MyClass() {
            MessageBox.Show("New MyClass instance created");
        }
    }

    public partial class MyClass {
        public void Test() {
            MessageBox.Show("OK");
        }
    }

    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {

            MyClass m = new MyClass();
            m.Test();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
